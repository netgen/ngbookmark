<?php
//
// Definition of ngBookmark class
//
// Created on: <29-Apr-2003 15:32:53 sp>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZ Publish
// SOFTWARE RELEASE: 4.4.0
// COPYRIGHT NOTICE: Copyright (C) 1999-2010 eZ Systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
// 
//   This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
// 
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//

/*! \file
*/

/*!
  \class ngBookmark ngbookmark.php
  \brief Handles bookmarking nodes for users

  Allows the creation and fetching of bookmark lists for users.
  The bookmark list is used in the browse page to allow quick navigation and selection.

  Creating a new bookmark item is done with
\code
$userID = eZUser::currentUserID();
$nodeID = 2;
$nodeName = 'Node';
ngBookmark::createNew( $userID, $nodeID, $nodeName )
\endcode

  Fetching the list is done with
\code
$userID = eZUser::currentUserID();
ngBookmark::fetchListForUser( $userID )
\endcode

*/

class ngBookmark extends eZPersistentObject
{
    function ngBookmark( $row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         "user_id" => array( 'name' => 'UserID',
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true,
                                                             'foreign_class' => 'eZUser',
                                                             'foreign_attribute' => 'contentobject_id',
                                                             'multiplicity' => '1..*' ),
                                         "node_id" => array( 'name' => 'NodeID',
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true,
                                                             'foreign_class' => 'eZContentObjectTreeNode',
                                                             'foreign_attribute' => 'node_id',
                                                             'multiplicity' => '1..*' ),
                                         "name" => array( 'name' => 'Name',
                                                          'datatype' => 'string',
                                                          'default' => '',
                                                          'required' => true ),
                                         "url" => array( 'name' => 'URL',
                                                         'datatype' => 'string',
                                                         'default' => '',
                                                         'required' => false ),
                                         "title" => array( 'name' => 'Title',
                                                           'datatype' => 'string',
                                                           'default' => '',
                                                           'required' => false ) ),
                      "keys" => array( "id" ),
                      "function_attributes" => array( 'node' => 'fetchNode',
                                                      'contentobject_id' => 'contentObjectID' ),
                      "increment_key" => "id",
                      "sort" => array( "id" => "asc" ),
                      "class_name" => "ngBookmark",
                      "name" => "ngbookmark" );

    }

    /*!
     \static
     \return the bookmark item \a $bookmarkID.
    */
    static function fetch( $bookmarkID )
    {
        return eZPersistentObject::fetchObject( ngBookmark::definition(),
                                                null, array( 'id' => $bookmarkID ), true );
    }

    /*!
     \static
     \return the bookmark list for user \a $userID.
    */
    static function fetchListForUser( $userID, $offset = false, $limit = false, $name = '' )
    {
        if ($name != '')
        {
          $objectList = eZPersistentObject::fetchObjectList( ngBookmark::definition(),
                                                            null,
                                                            array( 'user_id' => $userID, 'name' => $name ),
                                                            array( 'id' => 'desc' ),
                                                            array( 'offset' => $offset, 'length' => $limit ),
                                                            true );          
        }
        else
        {
          $objectList = eZPersistentObject::fetchObjectList( ngBookmark::definition(),
                                                            null,
                                                            array( 'user_id' => $userID ),
                                                            array( 'id' => 'desc' ),
                                                            array( 'offset' => $offset, 'length' => $limit ),
                                                            true ); 
        }
        
        return $objectList;
    }

    /*!
     \static
     Creates a new bookmark item for user \a $userID with node id \a $nodeID and name \a $nodeName.
     The new item is returned.
     \note Transaction unsafe. If you call several transaction unsafe methods you must enclose
     the calls within a db transaction; thus within db->begin and db->commit.
    */
    static function createNew( $userID, $nodeID, $nodeName, $url='', $title='' )
    {
        $db = eZDB::instance();
        $db->begin();
        $userID =(int) $userID;
        $nodeID =(int) $nodeID;
        $nodeName = $db->escapeString( $nodeName );
        $url = $db->escapeString($url);
        $title = $db->escapeString($title);

		$bookmarkToDelete = eZPersistentObject::fetchObjectList( ngBookmark::definition(),
                                                                 array('id', 'name', 'node_id', 'user_id', 'url', 'title'),
                                                                 array( 'user_id' => $userID, 'node_id' => $nodeID, 'url' => $url )
                                                                  );

		if ( is_array( $bookmarkToDelete ) )
		{
			foreach ( $bookmarkToDelete as $b )
			{
				$b->remove();
			}
		}

        //$db->query('DELETE FROM ngbookmark WHERE user_id=' . $userID . ' AND node_id=' . $nodeID . ' AND url="' . $url . '"');
			
        $bookmark = new ngBookmark( array( 'user_id' => $userID,
                                           'node_id' => $nodeID,
                                           'name' => $nodeName,
                                           'url' => $url,
                                           'title' => $title ) );
        $bookmark->store();
        $db->commit();
        return $bookmark;
    }

    /*!
     \return the tree node which this item refers to.
    */
    function fetchNode()
    {
        return eZContentObjectTreeNode::fetch( $this->attribute( 'node_id' ) );
    }

    /*!
     \return the content object ID of the tree node which this item refers to.
    */
    function contentObjectID()
    {
        $node = $this->fetchNode();
        if ( $node )
        {
            return $node->attribute( 'contentobject_id' );
        }

        return false;
    }

    /*!
     \static
     Removes all bookmark entries for all users.
     \note Transaction unsafe. If you call several transaction unsafe methods you must enclose
     the calls within a db transaction; thus within db->begin and db->commit.
    */
    static function cleanup()
    {
        $db = eZDB::instance();
        $db->query( "DELETE FROM ngbookmark" );
    }

    /*!
     \static
     Removes all bookmark entries for node and/or url.
     \note Transaction unsafe. If you call several transaction unsafe methods you must enclose
     the calls within a db transaction; thus within db->begin and db->commit.
    */
    static function removeByNodeID( $nodeID, $url='' )
    {
        $db = eZDB::instance();
        $nodeID =(int) $nodeID;
        if ($url!='')
            $bookmarkToDelete = eZPersistentObject::fetchObjectList( ngBookmark::definition(),
                                                                 array('id', 'name', 'node_id', 'user_id', 'url', 'title'),
                                                                 array( 'user_id' => $userID, 'node_id' => $nodeID, 'url' => $url )
                                                                  );

            if ( is_array( $bookmarkToDelete ) )
            {
                foreach ( $bookmarkToDelete as $b )
                {
                    $b->remove();
                }
            }     
        else
           $bookmarkToDelete = eZPersistentObject::fetchObjectList( ngBookmark::definition(),
                                                                 array('id', 'name', 'node_id', 'user_id', 'url', 'title'),
                                                                 array( 'user_id' => $userID, 'node_id' => $nodeID )
                                                                  );

            if ( is_array( $bookmarkToDelete ) )
            {
                foreach ( $bookmarkToDelete as $b )
                {
                    $b->remove();
                }
            }
    }
}

?>
