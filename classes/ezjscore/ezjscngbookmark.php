<?php

class ezjscNgBookmark
{
	public static function addObjects( $args )
	{
		/*if ( !is_array( $args ) || empty( $args ) )
			return array( 'status' => 'error', 'message' => 'Error adding search! Invalid parameters sent!' );
*/
		

		$http = eZHTTPTool::instance();
        
        /*if ( $http->hasPostVariable( 'nodeName' ) ) echo $http->postVariable( 'nodeName' );
        if ( $http->hasPostVariable( 'nodeID' ) ) echo $http->postVariable( 'nodeID' );
        if ( $http->hasPostVariable( 'ng_title' ) ) echo $http->postVariable( 'ng_title' );
        if ( $http->hasPostVariable( 'ng_url' ) ) echo $http->postVariable( 'ng_url' );
		*/
		if ( !$http->hasPostVariable( 'nodeID' ) || !$http->hasPostVariable( 'ng_url' ) || !$http->hasPostVariable( 'ng_title' ) )
			return array( 'status' => 'error', 'message' => 'Error adding search! Invalid parameters sent!' );

		$node = eZContentObjectTreeNode::fetch( (int) $http->postVariable('nodeID' ) );
		if ( !$node instanceof eZContentObjectTreeNode )
			return array( 'status' => 'error', 'message' => 'Error adding search! Invalid parameters sent!' );

		$nodeID = $node->attribute( 'node_id' );
		$title = $http->postVariable('ng_title');
		$url = '/content/view/full/' . $nodeID . $http->postVariable('ng_url');

		ngBookmark::createNew(
			eZUser::currentUserID(),
			$nodeID,
			$node->attribute( 'name' ),
			$url,
			$title
		);

        $user = eZUser::currentUser();
        $bookmarkObjects = ngBookmark::fetchListForUser( $user->id(), 0, 5, urldecode( $http->postVariable('nodeName') ) );
        $bookmarks = array();
        foreach ( $bookmarkObjects as $bookmarkObject )
        {
        	$bookmarks[] = array(
        		"title" => $bookmarkObject->attribute( 'title' ),
        		"url"   => $bookmarkObject->attribute( 'url' )
        	);
        }

		return array(
			'status'  => 'success',
			'message' => $title . ' is saved',
			'url'     => $url,
			'title'   => $title,
			'bookmarks' => $bookmarks
		);
	}
}

?>
