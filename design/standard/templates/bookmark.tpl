{def $page_limit=30
     $can_edit=false()}
<form action={concat("ngbookmark/bookmark/")|ezurl} method="post" >

<h1>{"My saved searches"|i18n("design/hitrorez")}</h1>

{def $bookmark_list=fetch('ngbookmark','bookmarks',hash('name', ''))}

{if $bookmark_list}

<p>
    {"These are the searches you have saved. Click on a link to view it."
      |i18n("design/hitrorez",,
            hash( '%emphasize_start', '<i>',
                  '%emphasize_stop', '</i>' ) )
      |nl2br}
</p>

{foreach $bookmark_list as $item}
  {if $item.node.object.can_edit}
    {set $can_edit=true()}
  {/if}
{/foreach}

<table class="list" width="100%" cellspacing="0" cellpadding="0" border="0">
<thead>
<tr>
    <th width="1">
    </th>
    <th width="69%">
        {"Name"|i18n("design/hitrorez")}
    </th>
    <th width="30%">
        {"Type"|i18n("design/hitrorez")}
    </th>
</tr>
</thead>
<tbody>
{foreach $bookmark_list as $item sequence array(bgdark,bglight) as $order}
<tr class="{$order}">
    <td align="left">
        <input type="checkbox" name="DeleteIDArray[]" value="{$item.id}" />
    </td>

    <td>
        <a href={urldecode($item.url)|ezurl}>{$item.title}</a>
    </td>

    <td>
        {$item.name|wash}
    </td>

</tr>
{/foreach}
</tbody>
</table>
<div class="buttonblock">
    <input class="button" type="submit" name="RemoveButton" value="{"Remove selected"|i18n('design/hitrorez')}" />
</div>

{else}

<div class="feedback">
    <h2>{"You have no saved searches"|i18n("design/hitrorez")}</h2>
</div>

{/if}

{*<div class="buttonblock">
    <input class="defaultbutton" type="submit" name="AddButton" value="{'Add searches'|i18n('design/standard/content/view')}" />
</div>*}


</form>
