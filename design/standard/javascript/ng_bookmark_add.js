jQuery(function($){
    var ngUrlGenerateResult;

    $('#bookmarksearch-open').click(function() {
        $('#bookmarksearch').removeClass().addClass('bookmarksearch-active');
    });

    $('#bookmarksearch-add').click(function() {
        var ngUrlGenerateResult = ngUrlGenerate();
        //$.ez( 'ezjscNgBookmark::addObjects::nodeName::' + encodeURIComponent($('#nodename').val()) + '::nodeID::' + ngUrlGenerateResult[0] + '::ng_title::' + $('#bookmarksearch-title').val() + '::ng_url::' + encodeURIComponent(ngUrlGenerateResult[1]),
            $.ez( 'ezjscNgBookmark::addObjects',
         {
            nodeName: $('#nodename').val(),
            nodeID: ngUrlGenerateResult[0],
            ng_title: $('#bookmarksearch-title').val(),
            ng_url: ngUrlGenerateResult[1]
        }, function(data) {
            $('#bookmarksearch-links').empty();
            for (var i in data.content.bookmarks) {
                $('#bookmarksearch-links').append('<li><a href="' + $('[name="ngbm-urlbase"]').val() + decodeURIComponent(data.content.bookmarks[i].url) + '">' + decodeURIComponent(data.content.bookmarks[i].title) + '</a></li>');
            }
            $('#bookmarksearch').removeClass().addClass('bookmarksearch-closed');
        });
    });
});

function ngUrlGenerate() {
    var generated_url = '/(show)/search';
    var defaultsearchnode = $('#sForm input[name="defaultsearchnode"]').val();
    var parentnode = '';

    $('#sForm').find('input:not([type="hidden"], [type="button"], [type="submit"], [type="reset"]), select').each(function(){
        if ($(this).val()) {
            generated_url = generated_url + '/(' + $(this).attr( 'name' ) + ')' + '/' + $(this).val();
        }
    });

    return [defaultsearchnode, generated_url];  
}
